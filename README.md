# coregulatory_network_CoRegNet


In this file you can find a R code that aims to reconstruct a large scale regulatory network from gene expression data and vizualize it. It's based on the tutorial found on bioconductor website for the CoRegNet package (link in the code).

Author : Marion Estoup

E-mail : marion_110@hotmail.fr

February 2023
